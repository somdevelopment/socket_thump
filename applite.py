from gevent import monkey
monkey.patch_all()

import cgi
from redis import Redis
from flask import Flask, render_template, request
from flask_socketio import SocketIO
from flask_cors import CORS, cross_origin

import json

app = Flask(__name__, static_url_path='', static_folder='web/static', template_folder='web/templates')
# app.debug = True
socketio = SocketIO(app, cors_allowed_origins='*')
redis = Redis(host='redis', port=6379)
# socketio = SocketIO(app, cors_allowed_origins='http://localhost:8080')
# db = redis.StrictRedis('localhost', 6379, 0)

@app.route('/')
def main():
    return render_template('index.html')

@socketio.on('connect', namespace='/users')
def ws_conn():
    # c = db.incr('connected')
    print("conected", request.sid)
    socketio.emit('msg', {'msg': 1}, namespace='/users', room=request.sid)

@socketio.on('disconnect', namespace='/users')
def ws_disconn():
    # c = db.decr('connected')
    print("disconnect", request.sid)
    socketio.emit('msg', {'msg': 2}, namespace='/users', room=request.sid)


@socketio.on('usuarios', namespace='/users')
def usuarios(message):
    # print(request.sid) # uuid del socket que realiza la peticion
    socketio.emit('usuarios', { 'data':'Hello world!' }, namespace='/users', room=None)



if __name__ == '__main__':
    server = "0.0.0.0"
    port = 5000
    print('Running in http://%s:%s' % (server,port))
    socketio.run(app, server, port=port)